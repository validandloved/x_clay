--[[
    X Clay. Adds hardened clay.
    Copyright (C) 2023 SaKeL <juraj.vajda@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to juraj.vajda@gmail.com
--]]

-- hardened clay
XClay:register_hardened_clay()

-- hardened clay smooth
XClay:register_hardened_clay_smooth()

if XClay.settings.x_clay_light_blue_wool then
    -- light blue wool
    minetest.register_node('x_clay:wool_light_blue', {
        description = XClay.colors.light_blue.name .. ' Wool',
        short_description = XClay.colors.light_blue.name .. ' Wool',
        tiles = { 'x_clay_wool_light_blue.png' },
        is_ground_content = false,
        groups = {
            snappy = 2,
            choppy = 2,
            oddly_breakable_by_hand = 3,
            flammable = 3,
            wool = 1,
            color_light_blue = 1
        },
        sounds = default.node_sound_defaults(),
    })
end
