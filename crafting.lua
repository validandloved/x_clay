--[[
    X Clay. Adds hardened clay.
    Copyright (C) 2023 SaKeL <juraj.vajda@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to juraj.vajda@gmail.com
--]]

local S = minetest.get_translator(minetest.get_current_modname())

-- light blue dye
minetest.register_craftitem('x_clay:dye_light_blue', {
    inventory_image = 'x_clay_dye_light_blue.png',
    description = S('Light Blue Dye'),
    short_description = S('Light Blue Dye'),
    groups = { dye = 1, color_light_blue = 1 }
})

minetest.register_craft({
    type = 'shapeless',
    output = 'x_clay:dye_light_blue 2',
    recipe = { 'group:dye,color_white', 'group:dye,color_blue' },
})

if XClay.settings.x_clay_light_blue_wool then
    -- light blue wool
    minetest.register_craft {
        type = 'shapeless',
        output = 'x_clay:wool_light_blue',
        recipe = { 'group:dye,color_light_blue', 'group:wool' },
    }
end
