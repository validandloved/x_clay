---@diagnostic disable: codestyle-check

---@class XClay
---@field colors table<string, ColorDef> Color table where `key` is color name and `value` is color HEX
---@field register_hardened_clay fun(self: XClay): nil Registers new node for hardware coloring
---@field settings {["x_clay_light_blue_wool"]: boolean}


---@class ColorDef
---@field name string|nil Color name
---@field hex string HEX color code
---@field craft_dye string|nil Dye for crafting recipe
