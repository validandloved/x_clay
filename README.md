# X Clay [x_clay]

Adds hardened clay and color variations of hardened clay.

![screenshot](screenshot.png)

## Features

- raw and smooth variations of hardened clay
- uses texture coloring
- optionally can add light blue wool
- supports stairs and slabs
- custom clay sounds

## Dependencies

- default

## Optional Dependencies

- stairs
- dye

## License:

### Code

GNU Lesser General Public License v2.1 or later (see included LICENSE file)

### Textures

**CC-BY-SA-4.0, by SaKeL**

- x_clay_hardened.png
- x_clay_hardened_smooth_overlay.png
- x_clay_hardened_overlay.png -- Derived from texture by Pixel Perfection by XSSheep (CC BY-SA 4.0)
- x_clay_wool_light_blue.png -- Derived from texture by Cisoun (CC BY-SA 3.0)
- x_clay_dye_light_blue.png -- Derived from texture Perttu Ahola (celeron55) <celeron55@gmail.com> (CC BY-SA 3.0)

### Sounds

**CC-BY-4.0, by nuFF3**, https://freesound.org

- x_clay_footstep.1.ogg
- x_clay_footstep.2.ogg
- x_clay_footstep.3.ogg
- x_clay_footstep.4.ogg
- x_clay_footstep.5.ogg
- x_clay_footstep.6.ogg

**CC-BY-4.0, by Anikadl**, https://freesound.org

- x_clay_dig.1.ogg
- x_clay_dig.2.ogg
- x_clay_dig.3.ogg
- x_clay_dig.4.ogg
- x_clay_dig.5.ogg
- x_clay_dig.6.ogg

**CC0-1.0, by kyles**, https://freesound.org, mixed with default_place_node_hard.1.ogg by Mito551 (CC-BY-SA 3.0)

- x_clay_place.1.ogg
- x_clay_place.2.ogg
- x_clay_place.3.ogg

**CC0-1.0, by Kinoton**, https://freesound.org, mixed with default_hard_footstep.1.ogg by Erdie (CC-BY-SA 3.0)

- x_clay_dug.1.ogg
- x_clay_dug.2.ogg
- x_clay_dug.3.ogg

## Installation

see: https://wiki.minetest.net/Installing_Mods
