--[[
    X Clay. Adds hardened clay.
    Copyright (C) 2023 SaKeL <juraj.vajda@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to juraj.vajda@gmail.com
--]]

local S = minetest.get_translator(minetest.get_current_modname())

---@type XClay
XClay = {
    colors = {
        base = { hex = '#997563' },
        black = { name = S('Black'), hex = '#2B2B2B' },
        dark_grey = { name = S('Dark Grey'), hex = '#4E4E4E' },
        grey = { name = S('Grey'), hex = '#A5A5A5' },
        white = { name = S('White'), hex = '#FFFFFF' },
        red = { name = S('Red'), hex = '#AB5C4A' },
        violet = { name = S('Violet'), hex = '#595287' },
        magenta = { name = S('Magenta'), hex = '#A25B5D' },
        pink = { name = S('Pink'), hex = '#FFA6A6' },
        dark_green = { name = S('Dark Green'), hex = '#556E48' },
        green = { name = S('Green'), hex = '#779154' },
        cyan = { name = S('Cyan'), hex = '#4E7683' },
        blue = { name = S('Blue'), hex = '#4B6696' },
        light_blue = { name = S('Light Blue'), hex = '#648CB4', craft_dye = 'group:dye,color_light_blue' },
        orange = { name = S('Orange'), hex = '#A86A4D' },
        yellow = { name = S('Yellow'), hex = '#BD8D39' },
        brown = { name = S('Brown'), hex = '#684E45' }
    },
    settings = {
        x_clay_light_blue_wool = minetest.settings:get_bool('x_clay_light_blue_wool', false) --[[@as boolean]]
    }
}

function XClay.register_hardened_clay(self)
    for color_id, color_def in pairs(self.colors) do
        local color_group = 'color_' .. color_id
        local def = {}

        -- Node
        local name = color_id ~= 'base' and 'x_clay:hardened_clay_' .. color_id or 'x_clay:hardened_clay'
        def.mod_origin = 'x_clay'
        def.description = color_def.name and color_def.name .. ' ' .. S('Hardened Clay') or S('Hardened Clay')
        def.short_description = def.description
        def.tiles = { 'x_clay_hardened.png^[colorize:' .. color_def.hex .. ':255^x_clay_hardened_overlay.png' }
        def.groups = { cracky = 3, hardenedclay = 1, hardenedclay_block = 1 }
        def.sounds = XClay.node_sound_clay()

        if color_id ~= 'base' then
            def.groups[color_group] = 1
        end

        minetest.register_node(name, def)

        -- Stairs / Slabs
        if minetest.get_modpath('stairs') then
            stairs.register_stair_and_slab(
                def.mod_origin .. '_' .. name,
                name,
                { cracky = 3, hardenedclay = 1 },
                def.tiles,
                def.description .. ' ' .. S('Stair'),
                def.description .. ' ' .. S('Slab'),
                XClay.node_sound_clay()
            )
        end

        -- Craft
        ---@type string
        local craft_dye = 'group:dye,' .. color_group

        if color_def.craft_dye then
            craft_dye = color_def.craft_dye
        end

        if color_id ~= 'base' then
            minetest.register_craft({
                output = name .. ' 8',
                recipe = {
                    {
                        'group:hardenedclay,hardenedclay_block',
                        'group:hardenedclay,hardenedclay_block',
                        'group:hardenedclay,hardenedclay_block'
                    },
                    {
                        'group:hardenedclay,hardenedclay_block',
                        craft_dye,
                        'group:hardenedclay,hardenedclay_block'
                    },
                    {
                        'group:hardenedclay,hardenedclay_block',
                        'group:hardenedclay,hardenedclay_block',
                        'group:hardenedclay,hardenedclay_block'
                    }
                }
            })
        else
            -- hardened clay
            if minetest.get_modpath('bakedclay') then
                minetest.register_craft({
                    output = 'x_clay:hardened_clay 8',
                    recipe = {
                        { 'default:clay', 'default:clay', 'default:clay' },
                        { 'default:clay', 'default:clay', 'default:clay' },
                        { 'default:clay', 'default:clay', 'default:clay' },
                    },
                })
            else
                minetest.register_craft({
                    type = 'cooking',
                    output = 'x_clay:hardened_clay 1',
                    recipe = 'default:clay'
                })
            end
        end
    end
end

function XClay.register_hardened_clay_smooth(self)
    for color_id, color_def in pairs(self.colors) do
        local color_group = 'color_' .. color_id
        local def = {}

        -- Node
        local recipe_name = color_id ~= 'base' and 'x_clay:hardened_clay_' .. color_id or 'x_clay:hardened_clay'
        local name = color_id ~= 'base' and 'x_clay:hardened_clay_' .. color_id .. '_smooth' or 'x_clay:hardened_clay_smooth'
        def.mod_origin = 'x_clay'
        def.description = color_def.name and color_def.name .. ' '
            .. S('Smooth Hardened Clay') or S('Smooth Hardened Clay')
        def.short_description = def.description
        ---@diagnostic disable-next-line: codestyle-check
        def.tiles = { 'x_clay_hardened.png^[colorize:' .. color_def.hex .. ':255^(x_clay_hardened_smooth_overlay.png^[invert:rgb)' }
        def.groups = { cracky = 3, hardenedclay_smooth = 1, hardenedclay_smooth_block = 1 }
        def.sounds = XClay.node_sound_clay()

        if color_id ~= 'base' then
            def.groups[color_group] = 1
        end

        minetest.register_node(name, def)

        -- Stairs / Slabs
        if minetest.get_modpath('stairs') then
            stairs.register_stair_and_slab(
                def.mod_origin .. '_' .. name,
                name,
                { cracky = 3, hardenedclay = 1 },
                def.tiles,
                def.description .. ' ' .. S('Stair'),
                def.description .. ' ' .. S('Slab'),
                XClay.node_sound_clay()
            )
        end

        -- Craft
        ---@type string
        local craft_dye = 'group:dye,' .. color_group

        if color_def.craft_dye then
            craft_dye = color_def.craft_dye
        end

        -- Craft
        if color_id ~= 'base' then
            minetest.register_craft({
                output = name .. ' 8',
                recipe = {
                    {
                        'group:hardenedclay_smooth,hardenedclay_smooth_block',
                        'group:hardenedclay_smooth,hardenedclay_smooth_block',
                        'group:hardenedclay_smooth,hardenedclay_smooth_block'
                    },
                    {
                        'group:hardenedclay_smooth,hardenedclay_smooth_block',
                        craft_dye,
                        'group:hardenedclay_smooth,hardenedclay_smooth_block'
                    },
                    {
                        'group:hardenedclay_smooth,hardenedclay_smooth_block',
                        'group:hardenedclay_smooth,hardenedclay_smooth_block',
                        'group:hardenedclay_smooth,hardenedclay_smooth_block'
                    }
                }
            })
        end

        minetest.register_craft({
            type = 'cooking',
            output = name,
            recipe = recipe_name
        })
    end
end

function XClay.node_sound_clay(self)
    local sounds = {
        footstep = { name = 'x_clay_footstep', gain = 0.2 },
        dig = { name = 'x_clay_dig', gain = 1.0 },
        dug = { name = 'x_clay_dug', gain = 5.0 },
        place = { name = "x_clay_place", gain = 1.0 }
    }

    return sounds
end
